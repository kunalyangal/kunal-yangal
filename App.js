import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import axios from "axios";
import "./App.scss";
import AddTodo from "./components/AddTodo";
import TodoList from "./components/TodoList";
import Accordion from "./components/Accordion";
import React from 'react';

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      todos: [],
    };
  }
  

  componentDidMount() {
    axios
      .get("/api")
      .then((response) => {
        this.setState({
          todos: response.data.data,
        });
      })
      .catch((e) => console.log("Error : ", e));
  }

  handleAddTodo = (value) => {
    axios
      .post("/api/todos", { text: value })
      .then(() => {
        this.setState({
          todos: [...this.state.todos, { text: value }],
        });
      })
      .catch((e) => console.log("Error : ", e));
  };

  render() {
    return (
      <div className="App container">
        <div className="container-fluid">
          <div className="row">
            <div className="col-xs-12 col-sm-8 col-md-8 offset-md-2">
              <h1>Todos for kunal</h1>
              <div className="todo-app">
                <AddTodo handleAddTodo={this.handleAddTodo} />
                <TodoList todos={this.state.todos} />
                <Accordion/>
                <Router>
        <div>
          <nav>
            <ul>
              <li>
                <Link to="/">App</Link>
              </li>
              <li>
                <Link to="/AddTodo">AddTodo</Link>
              </li>
              <li>
                <Link to="/TodoList">TodoList</Link>
              </li>
            </ul>
          </nav>
          <Switch>
          <Route path="/AddTodo">
              <AddTodo />
          </Route>
              <Route path="/TodoList">
              <TodoList />
          </Route>
               <Route path="/">
               <Home />
          </Route>
        </Switch>
      </div>
    </Router>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
