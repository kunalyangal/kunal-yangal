import React, { useState } from 'react'; 
   
const Example=()=> {
  const [change, setChange] = useState(true);      
      return (
        <div>
        <button onClick = {() => setChange(!change)}>
          Click Here!
        </button>
        {change?<h1>Welcome to Dezinefy</h1>:
                <h1>we provide best design service</h1>}
        </div>
        );
  }
  
export default Example;